import ipcalc
import sys
import signal
from scapy.all import *
import pandas as pd
import os
import threading
valid_hosts_ip = []
valid_hosts_mac = []

def signal_handler(signal, frame):
  #Handle Ctrl+C here
  print("")
  print("Stopping...")
  exit(0)


def usage():
        print("  Option    Description                 Required")
        print("--------------------------------------  ---------")
        print("  -t", "       IP or Network to scan        Yes")
        print("  -to", "      Timeout (Default 3)          No")
        print("  -e", "       Export result to file .xlsx  No")

class ARPthread(threading.Thread):
    '''Threaded ARP scan'''
    def __init__(self,ip,iface,timeout):
        self.ip = ip # host to scan
        self.iface = iface # the text from the textinput for the NIC
        self.timeout = timeout
        threading.Thread.__init__(self)
    try:
        def run(self):
            global valid_hosts_ip
            global valid_hosts_mac
            arprequest = Ether(dst='ff:ff:ff:ff:ff:ff')/ARP(pdst=str(self.ip), hwdst='ff:ff:ff:ff:ff:ff')
            arpresponse = srp1(arprequest, timeout=int(self.timeout), verbose=0, iface=str(self.iface))
            if arpresponse:
                valid_hosts_ip.append(str(arpresponse.psrc))
                valid_hosts_mac.append(str(arpresponse.hwsrc))
    except:
        pass


def arp_scan(IPs, time_out, interface):
	try:
		for ip in IPs:
			time.sleep(0.1)                        
			scanNode = ARPthread(ip,interface,time_out) # Create a thread
			scanNode.start() # Start the thread
	except:
		pass    

def run(params):
        input = ""
        interface = ""
        timeout = 3
        IPs= []
        export = False
        scan_hosts=[]
        global valid_hosts_ip
        global valid_hosts_mac

        signal.signal(signal.SIGINT, signal_handler) #Assign the signal handler


        if ("-t" in params):
                try:
                        input = params[params.index("-t")+1]
                except:
                        usage()
                        exit()
        else:
                print("Give IP or Network to scan ")
                usage()
                exit()
        if ("-i" in params):
                try:
                        interface = params[params.index("-i")+1]
                except:
                        usage()
                        exit() 
        if ("-to" in params):
                try:
                        timeout = int(params[params.index("-to")+1])
                except:
                        usage()
                        exit()
        if ("-e" in params):
                export = True
 
        if "/" in input:
                for x in ipcalc.Network(input):
                        IPs.append(str(x))
        else:
                IPs.append(input)
        for ip in IPs:
                scan_hosts.append(ip)
        print("Performing ARP scan... ")
        arp_scan(IPs, timeout, interface)
        if (len(valid_hosts_ip) == 0):
                print("No available devices")
                valid_hosts_ip.append("None")
                valid_hosts_mac.append("None")
        else:
                print("Available devices in the network:")
                print("IP" + " "*18+"MAC")
                for i in range(0,len(valid_hosts_ip)):
                        print("{:16}    {}".format(valid_hosts_ip[i],valid_hosts_mac[i]))
        if export==True:
                row = {'Scan hosts': [], 'IP available hosts': [], 'MAC available hosts': []}
                if (len(scan_hosts) > len(valid_hosts_ip)):
                        x = len(scan_hosts) - len(valid_hosts_ip)
                        for i in range(0,x):
                                valid_hosts_ip.append(" ")
                                valid_hosts_mac.append(" ")                   
                row['Scan hosts'] = scan_hosts
                row['IP available hosts'] = valid_hosts_ip
                row['MAC available hosts'] = valid_hosts_mac
                path = os.getcwd()[0:(int(os.getcwd().find("sdnrecon")))]
                filename=path + "sdnrecon/report/host_discovery/report_arp_scan.xlsx"
                df = pd.DataFrame(row)
                df.to_excel(filename, index = False, header=True)
                print("[##] Result saved to " + "/sdnrecon/report/host_discovery/report_arp_scan.xlsx")


def main():
        if (len(sys.argv) < 1):
                usage()
        else:
                params = sys.argv
                filter(None, params)
                params.pop(0)
                run(params)


if __name__ == '__main__':
        main()


