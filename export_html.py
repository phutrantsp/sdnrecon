import jinja2
import pandas as pd
from distutils.dir_util import copy_tree
import os

path = os.getcwd()[0:(int(os.getcwd().find("sdnrecon")))]

dfs_arpscan = pd.read_excel(path + "sdnrecon/report/host_discovery/report_arp_scan.xlsx", sheet_name= None)
dfs_arpscan = dfs_arpscan['Sheet1']
dfs_arpscan = dfs_arpscan.drop_duplicates(subset=['IP available hosts', 'MAC available hosts'], keep=False)
dfs_arpscan = dfs_arpscan.drop(columns="Scan hosts")

dfs_pingscan = pd.read_excel(path + "sdnrecon/report/host_discovery/report_ping_scan.xlsx", sheet_name= None)
dfs_pingscan = dfs_pingscan['Sheet1']
dfs_pingscan = dfs_pingscan.drop_duplicates(subset=["Available hosts"], keep=False)
dfs_pingscan = dfs_pingscan.drop(columns="Scan hosts")

dfs_controller_lldp = pd.read_excel(path + "sdnrecon/report/controller_detect/report_baseon_lldp.xlsx", sheet_name= None)
dfs_controller_lldp = dfs_controller_lldp['Sheet1']

dfs_controller_northbound = pd.read_excel(path + "sdnrecon/report/controller_detect/report_baseon_northbound.xlsx", sheet_name= None)
dfs_controller_northbound = dfs_controller_northbound['Sheet1']

dfs_controller_multi = pd.read_excel(path + "sdnrecon/report/controller_detect/report_multi_controller_detect.xlsx", sheet_name= None)
dfs_controller_multi = dfs_controller_multi['Sheet1']

dfs_portscan_tcpsyn = pd.read_excel(path + "sdnrecon/report/port_scan/report_simple_port_scan.xlsx", sheet_name= None)
dfs_portscan_tcpsyn = dfs_portscan_tcpsyn['Sheet1']

dfs_rule_icmp = pd.read_excel(path + "sdnrecon/report/rule_recontruct/report_rule_recontruct_icmp.xlsx", sheet_name= None)
dfs_rule_icmp = dfs_rule_icmp['Sheet1']

dfs_rule_tcp = pd.read_excel(path + "sdnrecon/report/rule_recontruct/report_rule_recontruct_tcp.xlsx", sheet_name= None)
dfs_rule_tcp = dfs_rule_tcp['Sheet1']


templateLoader = jinja2.FileSystemLoader(searchpath="./")
templateEnv = jinja2.Environment(loader=templateLoader)
TEMPLATE_FILE = "pdf_interest_report.html"
template = templateEnv.get_template(TEMPLATE_FILE)

#outputText = template.render(df_rule_tcp=dfs_rule_tcp,df_rule_icmp=dfs_rule_icmp,df_portscan_tcpsyn=dfs_portscan_tcpsyn, df_arpscan=dfs_arpscan, 
outputText = template.render(df_controller_northbound=dfs_controller_northbound,df_rule_tcp=dfs_rule_tcp,df_rule_icmp=dfs_rule_icmp,df_portscan_tcpsyn=dfs_portscan_tcpsyn, df_arpscan=dfs_arpscan, df_pingscan=dfs_pingscan,df_controller_lldp=dfs_controller_lldp,df_controller_multi=dfs_controller_multi)

html_file = open(path + 'sdnrecon/report/AutoRecon Report.html', 'w')
html_file.write(outputText)
html_file.close()
print("[##] Exported to /sdnrecon/report/AutoRecon Report.html")

#copy_tree("style", path + "sdnrecon/report/style")
