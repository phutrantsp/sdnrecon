import ipcalc
import sys
import signal
from scapy.all import *
import random
import pandas as pd
import os
from pythonping import ping
valid_hosts_ip = []

def signal_handler(signal, frame):
  #Handle Ctrl+C here
  print("")
  print("Stopping...")
  exit(0)
def info():
	print("Perform ping scan")

def usage():
        print("  Option    Description                  Required")
        print("---------------------------------------  --------")
        print("  -t", "       IP or Network to scan        Yes")
        print("  -to", "      Timeout (Default 1)          No")
        print("  -e", "       Export result to file .xlsx  No")

class Pingthread(threading.Thread):
    '''Threaded Ping scan'''
    def __init__(self,ip,timeout):
        self.ip = ip # host to scan
        self.timeout = timeout
        threading.Thread.__init__(self)
    try:
        def run(self):
            global valid_hosts_ip
            res = ping(self.ip, count=1, timeout=self.timeout)
            if "Reply from" in str(res):
                valid_hosts_ip.append(self.ip)
    except:
        pass



def ping_scan(IPs, time_out):
	try:
		for ip in IPs:
			time.sleep(0.3)
			scanNode = Pingthread(ip,time_out) # Create a thread
			scanNode.start() # Start the thread
	except:
		pass    


def run(params):
	input = ""
	timeout = 0.2
	IPs= []
	result = []
	export = False
	scan_hosts=[]

	signal.signal(signal.SIGINT, signal_handler) #Assign the signal handler

	if ("-t" in params):
		try:
			input = params[params.index("-t")+1]
		except:
			usage()
			exit()
	else:
		info()
		usage()
		exit()
	if ("-to" in params):
		try:	
			timeout = float(params[params.index("-to")+1])
		except:
			usage()
			exit()
	if ("-e" in params):
		export = True

	if "/" in input:
		for x in ipcalc.Network(input):
			IPs.append(str(x))
	else:
		IPs.append(input)
	#print("Scan hosts ")
	for ip in IPs:
		scan_hosts.append(ip)
	ping_scan(IPs, timeout)
	#time.sleep(5)
	if (len(valid_hosts_ip) == 0):
		print("No reachable host")
		valid_hosts_ip.append("None")
	else:
		print("Reachable hosts: ")
		for i in range(0,len(valid_hosts_ip)):
			print(valid_hosts_ip[i])
	if export==True:
		row = {'Scan hosts': [], 'Available hosts': []}
		if (len(scan_hosts) > len(valid_hosts_ip)):
			x = len(scan_hosts) - len(valid_hosts_ip)
			for i in range(0,x):
				valid_hosts_ip.append(" ")       
		row['Scan hosts'] = scan_hosts
		row['Available hosts'] = valid_hosts_ip
		path = os.getcwd()[0:(int(os.getcwd().find("sdnrecon")))]
		filename=path + "sdnrecon/report/host_discovery/report_ping_scan.xlsx"
		df = pd.DataFrame(row)
		df.to_excel(filename, index = False, header=True)
		print("[##] Result saved to " + "/sdnrecon/report/host_discovery/report_ping_scan.xlsx")
		

def main():
	if (len(sys.argv) < 1):
		usage()
	else:
		params = sys.argv
		filter(None, params)
		params.pop(0)
		run(params)


if __name__ == '__main__':
	main()























