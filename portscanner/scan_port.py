
import pandas as pd
import sys 
import socket
import os
from datetime import datetime 
ip_scan, open_ports, status = [],[],[]

def usage():
	print("Option    Description                                                Required")
	print("-------  ---------------------------------------------------------  ---------")
	print("-t", "       Target to scan                                             Yes")
	print("-p", "       Port to scan                                               No") 
	print("-e", "       Export result to file .txt                                 No")


def scan_port(target, ports):
	global ip_scan, open_ports, status
	oport = ""
	valid_target = False
	try:
		# re.match(r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$',target)
		ip = socket.inet_ntoa(socket.inet_aton(target))
		status.append('valid ip')
		valid_target = True
	except socket.error:
		# "If it is not an IP, assume it is a FQDN"
		try:
			ip = socket.gethostbyname(target)
			status.append('fqdn resolves')
			valid_target = True
		except socket.gaierror:
			ip = target
			status.append('fqdn does not resolve')
			open_ports.append("none")
	if valid_target:
		for port in ports:
			s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			socket.setdefaulttimeout(1)
			result = s.connect_ex((ip,port))
			if result == 0:
				oport = oport + " " + str(port)
				
			s.close()
		print("Open ports:" + oport)
		open_ports.append(oport)

def run(params):
	target = ""
	ports = ""
	export = False
	list_targets=[]
	global ip_scan, open_ports, status
	if ("-t" in params):
		try:
			target = params[params.index("-t")+1]
		except:
			usage()
			exit()
	else:
		usage()
		exit()

	if ("," in target):
		list_targets=target.split(",")
	else:
		list_targets.append(target)

	if ("-e" in params):
		export = True

	if ("-p" in params):
		try:
			ports = (params[params.index("-p")+1]).lower()
		except:
			usage()
			exit()
		if (ports.isdigit() == True):
			list_ports = []
			list_ports.append(int(ports))
		elif ("," in ports):
			list_ports = []
			for p in ports.split(","):
				list_ports.append(int(p))
		else:
			usage()
			exit()


	for t in list_targets:
		ip_scan.append(str(t))
		print("Scanning port on " + str(t))
		scan_port(t, list_ports)

	if export == True:
		row = {'IP': [], 'Status': [], 'Open ports': []}
		row['IP'] = ip_scan
		row['Status'] = status
		row['Open ports'] = open_ports
		path = os.getcwd()[0:(int(os.getcwd().find("sdnrecon")))]
		filename=path + "sdnrecon/report/port_scan/report_simple_port_scan.xlsx"
		df = pd.DataFrame(row)
		df.to_excel(filename, index = False, header=True)
		print("[##] Result saved to " + "/sdnrecon/report/port_scan/report_simple_port_scan.xlsx")


def main():
        if (len(sys.argv) < 1):
                usage()
                exit()
        else:
                params = sys.argv
                filter(None, params)
                params.pop(0)
                run(params)


if __name__ == '__main__':
        main()


 
